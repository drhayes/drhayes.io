---
title: 'About'
menu: 'main'
---

My name is David Hayes. I do stuff with computers. I live in Austin, TX.

This site is made with some help from these technologies and media:

* [Hugo][hugo]
* [PrismJS][prism]
* [Markdown][markdown]

[hugo]: https://gohugo.io
[prism]: http://prismjs.com/
[markdown]: https://daringfireball.net/projects/markdown/
